function I=Trapecios(f,a,b,n)
    % Fórmula cerrada de Newton-Cotes para nodos equiespaciados
    % Fórmula de trapecios compuesta para una función conocida
    h=(b-a)/n;
    x=a:h:b;
    pesos=[1 2*ones(1,n-1) 1];
    I=h/2*sum(pesos.*f(x));
end