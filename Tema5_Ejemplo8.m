% Gauss Laguerre:
% Polinomio de grado 3

syms x
% p1=1; p2=1-x; p_{k+2}=(2k+3-x)*p_{k+1}-(k+1)^2*p_k
p3=-x.^3+9*x.^2-18*x+6;
% raíces
xi=double(solve(p3==0));
% coeficientes
% ci=n!^2*xi/(p^2_{n+1}(xi))
p4=x.^4-16*x.^3+72*x.^2-96*x+24;
ci=(factorial(3))^2*xi./(double(subs(p4,x,xi))).^2;
f=@(y) sin(y/10);
I=1/10*sum(f(xi).*ci);