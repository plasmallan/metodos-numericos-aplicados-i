function D2 = poly_interp_second_derivative(alpha_rad, beta, ai)
    % Considerando los tres puntos más cercanos al punto alpha_interp
    xi=ai;
    n = length(alpha_rad);
    [~, idx] = sort(abs(alpha_rad - ai));
    idx = idx(1:3); % Índices de los tres puntos más cercanos
    x = alpha_rad(idx);
    y = beta(idx);
    n = length(x);
    D2 = 0;
    for i = 1:n
        product = y(i);
        for j = 1:n
            if j ~= i
                product = product * (2 * xi - x(j) - x(i)) / (x(i) - x(j));
            end
        end
        D2 = D2 + product;
    end
end