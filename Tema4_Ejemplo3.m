% Diferencias progresivas
h=(1-0)/10;
x=0:h:1;
f=x.^2.*exp(-x);
df_exacta=2*x.*exp(-x)-x.^2.*exp(-x);
n=length(x);
dph=1/h*(f(2:end)-f(1:end-1));
dph2=1/(2*h)*(-f(3:end)+4*f(2:end-1)-3*f(1:end-2));
figure;
hold on
plot(x(1:n-1),dph,'-o')
plot(x(1:n-2),dph2,'-o')
plot(x(1:n-1),df_exacta(1:n-1),'-')
legend('Primer orden', 'Segundo orden','Exacta')
hold off
% Diferencias regresivas
drh=1/h*(f(2:end)-f(1:end-1));
drh2=1/(2*h)*(3*f(3:end)-4*f(2:end-1)+f(1:end-2));
figure;
hold on
plot(x(2:n),drh,'-o')
plot(x(3:n),drh2,'-o')
plot(x(2:n),df_exacta(2:n),'-')
legend('Primer orden', 'Segundo orden','Exacta')
hold off