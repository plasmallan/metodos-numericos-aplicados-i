function p=polinomioHermite(xi,fi,dfi)
    xi=xi(:);
    fi=fi(:);
    dfi=dfi(:);
    n=2*length(xi);
    syms x
    col=2;
    p=fi(1);
    vx=1;
    z=repelem(xi,2);
    F=repelem(fi,2);
    while col<=n
        if col==2
            m=n/2;
            num=fi(2:(m-col+2),col-1)-fi(1:(m-col+1),col-1);
            den=(xi(col:m)-xi(1:(m-col+1)));
            J(1:m-col+1,1)=num./den;
            for i=1:m-1
                F(2*i-1,col)=dfi(i);
                F(2*i,col)=J(i);
            end
            F(2*m-1,col)=dfi(m);
        else
            num=F(2:(n-col+2),col-1)-F(1:(n-col+1),col-1);
            den=(z(col:n)-z(1:(n-col+1)));
            F(1:n-col+1,col)=num./den;
        end
        vx=vx*(x-z(col-1));
        p=p+F(1,col)*vx;
        col=col+1;
    end
end