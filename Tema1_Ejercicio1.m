f1=@(x) x.^2-x;
f2=@(x) cos(x).^2;
f3=@(x) exp(-x);
f4=@(x) sin(x)+cos(x);

x=linspace(-5,5,11);
figure;
plot(x,f1(x),"b")
figure;
plot(x,f2(x),"r")
figure;
plot(x,f3(x),"g")
figure;
plot(x,f4(x),"c")