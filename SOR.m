function [x,iter,incre]=SOR(w,A,b,x0,tol,maxiter)
    b=b(:);
    x0=x0(:);
    iter=0;
    incre=tol+1;
    D=diag(diag(A));
    L=tril(A,-1);
    U=triu(A,1);
    xGS=x0;
    % H=M^{-1}N
    % max(eig(H))
    % w=2/(1+sqrt(1-rho(GS))
    while iter<maxiter && incre>tol
        B=D+L;
        d=b-U*x0;
        xGS(1)=d(1)/B(1,1);
        for j=2:length(d)
            xGS(j)=(d(j)-B(j,1:j-1)*xGS(1:j-1))/B(j,j);
        end
        x=(1-w)*x0+w*xGS;
        % incre=norm(b-A*x);
        % incre=norm(x-x0,inf);
        incre=norm(x-x0,inf)/norm(x0,inf);
        iter=iter+1;
        x0=x;
    end
    if incre>tol
        disp("Necesito más iteraciones");
    end
end