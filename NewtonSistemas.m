function [sol, iter, ACOC, incre1, incre2]=NewtonSistemas(F,x0,tol,maxit)
    digits(200);
    % Inicialización de las variables
    iter=1;
    incre1=tol+1;
    incre2=tol+1;
    x0=x0(:);
    [Fx,dFx]=feval(F,x0);
    % Criterio de parada
    while incre1+incre2>tol && iter<maxit
        % Expresión del método de Newton
        x=x0-dFx\Fx;
        % vpa(x,5);
        incre1=norm(x-x0);
        % incX=vpa(incre1,5)
        I(iter)=incre1;
        % Actualización de extimación inicial
        x0=x;
        [Fx,dFx]=feval(F,x0);
        incre2=norm(Fx);
        % incF=vpa(incre2,5)
        % pause
        iter=iter+1;
    end
    iter=iter-1;
    if length(I)>2
        sol=x;
        ACOC=log(I(3:end)./I(2:end-1))./log(I(2:end-1)./I(1:end-2));
    else
        disp("Necesito más iteraciones");
    end
    sol=vpa(x0,6);
    incre2=vpa(incre2,5);
    incre1=vpa(incre1,5);
    ACOC=vpa(ACOC,6);
end