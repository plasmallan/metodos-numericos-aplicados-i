% Datos del problema
a = 100; % mm
b = 120; % mm
c = 150; % mm
d = 180; % mm
alpha_deg = [0, 5, 10, 15, 20, 25, 30]; % Ángulos en grados
beta = [1.6595, 1.5434, 1.4186, 1.2925, 1.1712, 1.0585, 0.9561]; % Valores correspondientes de beta
omega_AB = 25; % Velocidad angular constante de AB en rad/s
alpha_interp = 12; % Ángulo para interpolación en grados

% Conversión de ángulos a radianes
alpha_rad = deg2rad(alpha_deg);
db_da=Diferencias(alpha_rad,beta);
db_dt=omega_AB*db_da;
% Interpolación de Lagrange para encontrar beta en alpha_interp
beta_interp = lagrange_interpolation(alpha_rad, beta, deg2rad(alpha_interp));



% Interpolación polinomial para calcular la derivada segunda de alpha con respecto a beta en alpha_interp
D2_alpha = calculateSecondDerivative(alpha_rad, beta, deg2rad(alpha_interp));

% Mostrar resultados
disp(['Valores d(beta)/dalpha: ', num2str(db_da)]);
disp(['Valores d(beta)/dt: ', num2str(db_dt)]); % d(beta)/dt=d(beta)/dalpha*d(alpha)/dt
disp(['Valor interpolado de beta para alpha = 12 grados: ', num2str(beta_interp)]);
disp(['Segunda derivada de alpha con respecto a beta en alpha = 12 grados: ', num2str(D2_alpha)]);

% Función para interpolación de Lagrange
function result = lagrange_interpolation(x, y, xi)
    result = 0;
    n = length(x);
    for i = 1:n
        term = y(i);
        for j = 1:n
            if j ~= i
                term = term * (xi - x(j)) / (x(i) - x(j));
            end
        end
        result = result + term;
    end
end

% Función para interpolación polinomial para calcular la derivada segunda

function alpha_second_derivative = calculateSecondDerivative(alpha_rad, beta, ai)
    % Encuentra los índices de los puntos más cercanos a alpha_target
    [~, idx] = sort(abs(alpha_rad - ai));
    closest_points = idx(1:3);

    % Extrae los puntos más cercanos
    alpha_closest = alpha_rad(closest_points);
    beta_closest = beta(closest_points);

    % Realiza la interpolación polinomial
    poly_coeff = polyfit(beta_closest, alpha_closest, 2);  % Polinomio de grado 2

    % Calcula la segunda derivada del polinomio
    second_derivative_coeff = polyder(polyder(poly_coeff));

    % Evalúa la segunda derivada en el punto alpha_target
    beta_target = beta(idx(1));  % El valor de beta correspondiente a alpha_target
    alpha_second_derivative = polyval(second_derivative_coeff, beta_target);
end

% Cálculo de diferencias progresivas, regresivas y centrales para d(beta)/dt
function d_dt = Diferencias(alpha_rad,beta)
    d_alpha = diff(alpha_rad);
    d_alpha(length(alpha_rad)) = d_alpha(1);
    d_beta=zeros([1 length(beta)]);
    d_dt=zeros([1 length(beta)]);
    for i = 1:length(alpha_rad)
        % Diferencia progresiva
        if i==1
            d_beta(1)=4*beta(2)-3*beta(1)-beta(3);
            d_dt(1)=d_beta(1)/2/d_alpha(1);
        % Diferencia regresiva
        elseif i==length(alpha_rad)
            d_beta(length(alpha_rad))=3*beta(length(alpha_rad))-4*beta(length(alpha_rad)-1)+beta(length(alpha_rad)-2);
            d_dt(length(alpha_rad))=d_beta(length(alpha_rad))/2/d_alpha(length(alpha_rad));
        % Diferencia centrada
        elseif i~=1 && i<length(alpha_rad)
            d_beta(i)=beta(i+1)-beta(i-1);
            d_dt(i)=d_beta(i)/2/d_alpha(i);
        end
    end
end