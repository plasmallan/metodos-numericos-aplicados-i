% PVI ecuaciones diferenciales de primer orden
% Utilizando ODE23
h=0.1;

% Se define la variable independiente
a=0;b=3;h=0.1;
td=a:h:b;
% Condiciones iniciales
y0=1;
[t,y]=ode23(@PVI,td,y0);
figure;
plot(td,y,'o-')

% Se define el sistema
function dy=PVI(t,y)
    dy=(1-2*t).*y;
end