% Cálculo de diferencias progresivas, regresivas y centrales para d(beta)/dt
function d_dt = Diferencias(alpha_rad,beta)
    d_alpha = diff(alpha_rad);
    d_alpha(length(alpha_rad)) = d_alpha(1);
    d_beta=zeros([1 length(beta)]);
    d_dt=zeros([1 length(beta)]);
    for i = 1:length(alpha_rad)
        % Diferencia progresiva
        if i==1
            d_beta(1)=4*beta(2)-3*beta(1)-beta(3);
            d_dt(1)=d_beta(1)/2/d_alpha(1);
        % Diferencia regresiva
        elseif i==length(alpha_rad)
            d_beta(length(alpha_rad))=3*beta(length(alpha_rad))-4*beta(length(alpha_rad)-1)+beta(length(alpha_rad)-2);
            d_dt(length(alpha_rad))=d_beta(length(alpha_rad))/2/d_alpha(length(alpha_rad));
        % Diferencia centrada
        elseif i~=1 && i<length(alpha_rad)
            d_beta(i)=beta(i+1)-beta(i-1);
            d_dt(i)=d_beta(i)/2/d_alpha(i);
        end
    end
end