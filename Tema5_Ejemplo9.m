% Gauss-Hermite:
% p1=1; p2=2*x; p_{k+2}=2*x*p_{k+1}-2*(k+1)*p_k
% coeficientes
% ci=2^{n-1}*n!*sqrt(pi)/n^2/p^2_{n-1}(xi)
syms x
p3=8*x.^3-12*x;
p4=16*x.^4-48*x.^2+12;
xi=double(solve(p4==0));
ci=2^(4-1)*factorial(4)*sqrt(pi)/4^2./(double(subs(p3,x,xi)).^2);
f=@(y) abs(y);
I=1/4*sum(ci.*f(xi));