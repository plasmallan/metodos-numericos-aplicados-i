syms x;
f1=x^3-3*x+2;
df1=diff(f1,x);
disp("La derivada analítica es")
disp(df1)
x=linspace(-1,1,11);
f1=subs(f1);
df1=subs(df1);
disp("El valor de la derivada analítica es")
disp(df1)
f1=@(x) x.^3-3*x+2;
h=0.0001;
df1=(f1(x+h)-f1(x))/h;
disp("El valor de la derivada numérica es")
disp(df1)
