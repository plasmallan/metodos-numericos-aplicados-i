function [t,y]=ABM4_Sistemas(f,a,b,N,ya)
    h=(b-a)/N;
    t=a:h:b;
    t=t(:);
    y=zeros(N+1,length(ya));
    y(1,:)=ya;
    % RK4
    for k=1:3
        ff(k,:)=feval(f,t(k),y(k,:));
        k1=ff(k,:);
        k2=feval(f,t(k)+h/2,y(k,:)+h*k1/2);
        k3=feval(f,t(k)+h/2,y(k,:)+h*k2/2);
        k4=feval(f,t(k+1),y(k,:)+h*k3);
        y(k+1,:)=y(k,:)+h*(k1+2*k2+2*k3+k4)/6;
    end
    % Siguientes pasos con AB4
    for k=4:N
        % Predictor AB4
        ff(k,:)=feval(f,t(k),y(k,:));
        yp=y(k,:)+h/24*(55*ff(k,:)-59*ff(k-1,:)+37*ff(k-2,:)-9*ff(k-3,:));
        % Corrector AM4
        y(k+1,:)=y(k,:)+h/24*(feval(f,t(k-2),y(k-2,:))-5*feval(f,t(k-1,:),y(k-1,:))+19*feval(f,t(k),y(k,:))+9*feval(f,t(k+1),yp));
    end
end