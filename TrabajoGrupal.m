H=772e3; % m
R=6378.14e3; % m
v0=6700; % m/s
G=6.672e-11; % m3 kg/s
M=5.9742e24; % kg
r0=R+H;
rdot0=0;
theta0=0;
thetadot0=v0/r0;
y0=[r0,rdot0,theta0,thetadot0]';

% Funciones para método de Euler Explícito
f1=@(y,dt) y(1)+dt*y(2);
f2=@(y,dt) y(2)+y(1)*y(4)^2*dt-G*M*dt/y(1)^2;
f3=@(y,dt) y(3)+y(4)*dt;
f4=@(y,dt) y(4)-2*y(2)*y(4)*dt/y(1);

% Cálculo de vector de estado
[yE,tE]=EulerExplicito4(f1,f2,f3,f4,y0,1200,0,1000);
[yR,tR]=RungeKutta4(@Sistema4,y0,1200,0,1000);

[tODE45, yODE45] = ode45(@Sistema4, [0, 1200], y0);

% Representación de espacios de fase
% Espacio de fase (r, rdot)
figure;
plot(yR(1, :), yR(2, :), 'b', 'LineWidth', 1.5);  % Runge-Kutta en azul
hold on;
plot(yE(1, :), yE(2, :), 'r', 'LineWidth', 1.5);  % Euler en rojo
plot(yODE45(:, 1), yODE45(:, 2), 'g', 'LineWidth', 1.5);  % ode45 en verde
title('Espacio de Fase (r, rdot)');
xlabel('r');
ylabel('rdot');
legend('Runge-Kutta', 'Euler', 'ode45');
hold off;
% Espacio de fase (theta, thetadot)
figure;
plot(yR(3, :), yR(4, :), 'b', 'LineWidth', 1.5);  % Runge-Kutta en azul
hold on;
plot(yE(3, :), yE(4, :), 'r', 'LineWidth', 1.5);  % Euler en rojo
plot(yODE45(:, 3), yODE45(:, 4), 'g', 'LineWidth', 1.5);  % ode45 en verde
title('Espacio de Fase (theta, thetadot)');
xlabel('theta');
ylabel('thetadot');
legend('Runge-Kutta', 'Euler', 'ode45');
hold off;

% Velocidad de impacto
t_impacto_E = interp1(yE(1,[2:1001]), tE, R);
t_impacto_R = interp1(yR(1,[2:1001]), tR, R);
t_impacto_ODE45 = interp1(yODE45(:,1), tODE45, R);

disp(['Tiempo de Impacto por Euler (s): ', num2str(t_impacto_E)]);
disp(['Tiempo de Impacto por Runge-Kutta 4 (s): ', num2str(t_impacto_R)]);
disp(['Tiempo de Impacto por ODE45 (s): ', num2str(t_impacto_ODE45)]);

v_impacto_E = interp1(tE,yE(2,[2:1001]), t_impacto_E);
v_impacto_R = interp1(tR,yR(2,[2:1001]), t_impacto_R);
v_impacto_ODE45 = interp1(tODE45,yODE45(:,2), t_impacto_ODE45);

disp(['Velocidad de Impacto por Euler (m/s): ', num2str(v_impacto_E)]);
disp(['Velocidad de Impacto por Runge-Kutta 4 (m/s): ', num2str(v_impacto_R)]);
disp(['Velocidad de Impacto por ODE45 (m/s): ', num2str(v_impacto_ODE45)]);

% Sistema como función para Runge Kutta 4
function dydt = Sistema4(t,y)
    G=6.672e-11; % m3 kg/s
    M=5.9742e24; % kg
    dydt = zeros(4, 1);
    dydt(1) = y(2);
    dydt(2) = y(1) * y(4)^2 - G * M / y(1)^2;
    dydt(3) = y(4);
    dydt(4) = -2 * y(2) * y(4) / y(1);
end

% Funciones de solución de sistema
function [y,t]=EulerExplicito4(f1,f2,f3,f4,y0,tf,ti,n)
    t=linspace(ti,tf,n);
    dt=t(2)-t(1);
    y=zeros(length(y0),length(t));
    y(:,1)=y0;
    for i=1:length(t)
       y(1,i+1)=f1(y(:,i),dt);
       y(2,i+1)=f2(y(:,i),dt);
       y(3,i+1)=f3(y(:,i),dt);
       y(4,i+1)=f4(y(:,i),dt);
    end
end
function [y,t]=RungeKutta4(sistema,y0,tf,ti,n)
    t=linspace(ti,tf,n);
    dt=t(2)-t(1);
    y=zeros(length(y0),length(t));
    y(:,1)=y0;
    for i=1:length(t)
        k1=dt*sistema(t(i),y(:,i));
        k2=dt*sistema(t(i)+dt/2,y(:,i)+k1/2);
        k3=dt*sistema(t(i)+dt/2,y(:,i)+k2/2);
        k4=dt*sistema(t(i)+dt,y(:,i)+k3);
        y(:,i+1)=y(:,i)+(k1+2*k2+2*k3+k4)/6;
    end
end