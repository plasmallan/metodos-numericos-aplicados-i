function [t,y]=Euler(f,a,b,N,y0)
    % Código para resolver PVI con método de Euler
    h=(b-a)/N; % tamaño de paso
    t=a:h:b;
    t=t(:);
    % Se inicializa la solución
    y=zeros(N+1,1);
    % Condición inicial
    y(1)=y0;
    % Se entra al bucle
    for k=1:N
        y(k+1)=y(k)+h*feval(f,t(k),y(k));
    end
end