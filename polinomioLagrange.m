function p=polinomioLagrange(xi,fi)
    xi=xi(:);
    fi=fi(:);
    n=length(xi);
    syms x
    col=1;
    while col<=n
        L(col)=prod(x-xi([1:col-1,col+1:n]))/prod(xi(col)-xi([1:col-1,col+1:n]));
        col=col+1;
    end
    p=sum(L*fi);
end