xi=0:5;
fi=[36.8 37.2 38.3 37.9 37.7 37.1];
[b,p]=polinomioNewton(xi,fi);
pretty(p)
t=linspace(0,5);
syms x
f=double(subs(p,x,t));
figure;
hold on;
grid on;
plot(t,f)
plot(xi,fi,"o")
hold off;
T=double(subs(p,x,2.75));