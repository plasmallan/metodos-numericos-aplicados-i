function [sol, iter, ACOC, incre1, incre2]=JarratSistemas(F,x0,tol,maxit)
    digits(200);
    % Inicialización de las variables
    iter=0;
    incre1=tol+1;
    x0=x0(:);
    [Fx0,dFx0]=feval(F,x0);
    incre2=norm(Fx0);
    I=[];
    % Criterio de parada
    while incre1+incre2>tol && iter<maxit
        % Expresión del método de Jarrat
        v=dFx0\Fx0;
        y=x0-2/3*v;
        [Fy,dFy]=feval(F,y);
        z=(3*dFy-dFx0)\(3*dFy+dFx0);
        x=x0-1/2*z*v;
        [Fx,dFx]=feval(F,x);
        % vpa(x,5);
        % incX=vpa(incre1,5)
        incre1=norm(x-x0);
        I=[I,incre1];
        incre2=norm(Fx);
        iter=iter+1;
        % Actualización de extimación inicial
        x0=x; Fx0=Fx; dFx0=dFx;
    end
    iter=iter-1;
    if length(I)>2
        sol=x;
        ACOC=log(I(3:end)./I(2:end-1))./log(I(2:end-1)./I(1:end-2));
    else
        disp("Necesito más iteraciones");
    end
    sol=vpa(x0,6);
    incre2=vpa(incre2,5);
    incre1=vpa(incre1,5);
    ACOC=vpa(ACOC,6);
end