function [x,iter,incre]=GaussSeidel(A,b,x0,tol,maxiter)
    b=b(:);
    x0=x0(:);
    x=x0;
    iter=0;
    incre=tol+1;
    D=diag(diag(A));
    L=tril(A,-1);
    U=triu(A,1);
    M=D+L;
    N=-U;
    while iter<maxiter && incre>tol
        d=b-U*x0;
        x(1)=d(1)/M(1,1);
        for j=2:length(d)
            x(j)=(d(j)-M(j,1:j-1)*x(1:j-1))/M(j,j);
        end
        % incre=norm(b-A*x);
        % incre=norm(x-x0,inf);
        incre=norm(x-x0,inf)/norm(x0,inf);
        iter=iter+1;
        x0=x;
    end
    if incre>tol
        disp("Necesito más iteraciones");
    end
end