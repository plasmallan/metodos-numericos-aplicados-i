% Diferencias finitas centrales
h=0.1;
x=0:h:1;
f=x.^2.*exp(-x);
n=length(x);
for i=2:n-1
    df1(i)=(f(i+1)-f(i-1))/(2*h);
end
df2(2:n-1)=(f(3:n)-f(1:n-2))/(2*h); % Como vector
df_exacta=2*x.*exp(-x)-x.^2.*exp(-x);
hold on
plot(x(2:n-1),df1(2:n-1),"o-")
plot(x(2:n-1),df_exacta(2:n-1),"-")
hold off