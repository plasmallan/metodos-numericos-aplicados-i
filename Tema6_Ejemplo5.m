f=@PVI;
y_exact=@(t) (exp(0.25-(0.5-t).^2));
a=0;
b=3;
y0=1;

N=8;
[t8,y8]=Euler(f,a,b,N,y0);
E8=max(abs(y_exact(t8)-y8));
figure;
plot(t8,y8)

N=16;
[t16,y16]=Euler(f,a,b,N,y0);
E16=max(abs(y_exact(t16)-y16));
figure;
plot(t16,y16)

function dy=PVI(t,y)
    dy=(1-2*t)*y;
end