function [t,y]=EulerImplicito(f,a,b,N,y0,tol,maxiter)
    h=(b-a)/N;
    t=a:h:b;
    t=t(:);
    y=zeros(N+1,1);
    y(1)=y0;
    for k=1:N
        x0=y(k);
        dif=tol+1;
        iter=1;
        while dif>tol && iter<maxiter
            [fx0,dfx0]=feval(f,t(k+1),x0);
            g=x0-y(k)-h*fx0;
            dg=1-h*dfx0;
            x1=x0-g/dg;
            dif=abs(x1-x0);
            x0=x1;
            iter=iter+1;
        end
        y(k+1)=y(k)+h*feval(f,t(k+1),x0);
    end
end