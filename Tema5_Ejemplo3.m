h=1.52;
x=0+h/2:h/2:9.12-h/2; % Está sospechoso
F=[0 40.04 57.83 62.28 46.71 53.38 22.24];
alpha=[0.5 1.40 0.75 0.90 1.30 1.48 1.50];
f=F.*cos(alpha);
% Se aplica punto medio
I=2*h*sum(f(1:2:end))