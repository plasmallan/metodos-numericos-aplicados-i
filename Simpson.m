function I=Simpson(f,a,b,n)
    % Fórmula cerrada de Newton-Cotes para nodos equiespaciados
    % fórmula de Simpson compuesta para una función conocida
    h=(b-a)/n;
    x=a:h:b;
    pesos=ones(1,n+1);
    pesos(2:2:n)=4;
    pesos(3:2:n-1)=2;
    I=h/3*sum(pesos.*f(x));
end