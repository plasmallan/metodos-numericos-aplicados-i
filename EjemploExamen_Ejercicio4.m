[ci,xi]=GaussLaguerre(80);
syms y
f=@(y) (cos(sqrt(y))./(2.*sqrt(y)));
I=sum(f(xi).*ci);
syms x
g=@(x) exp(-x.^2).*cos(x);
I_exact=integral(g,0,Inf);
error=abs(I-I_exact);