a=0;
b=2;
h=0.01;
N=(b-a)/h;
ya=[0,0.25];
f=@PVI;
[t1,y1]=AB2_Sistemas(f,a,b,N,ya);
[t2,y2]=ABM4_Sistemas(f,a,b,N,ya);
[t3,y3]=EulerSistemas(f,a,b,N,ya);
figure;
hold on;
plot(t1,y1(:,1));
plot(t2,y2(:,1));
plot(t3,y3(:,1));
legend("AB2","ABM4","Euler");
hold off;
function dy=PVI(t,theta)
    t1=theta(1);
    t2=theta(2);
    dy=[t2,9.81/0.5*t1];
end