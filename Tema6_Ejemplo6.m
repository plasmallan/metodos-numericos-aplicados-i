f=@MalthusPVI;
a=0;
b=3;
y0=1;
N=20;
[tE,yE]=Euler(@(t,y)(-10*y),a,b,N,y0);
[tI,yI]=EulerImplicito(f,a,b,N,y0,0.00001,20);
y_exact=@(t) exp(-10*t);
figure;
hold on
plot(tE,yE,"-o")
plot(tI,yI,"-o")
plot(tI,y_exact(tI),"-")
legend("Explicito","Implicito","Exacto")
hold off
function [fun,dfun]=MalthusPVI(t,y)
    k=-10;
    fun=k*y;
    dfun=k;
end