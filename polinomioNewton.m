function [b,p]=polinomioNewton(xi,fi)
    % Función que calcula el polinomio de Newton
    % Entrada (xi,fi): nodos
    % Salida b, p coeficientes del polinomio y polinomio
    xi=xi(:); % Formato columna
    fi=fi(:);
    n=length(xi);
    syms x
    col=2;
    p=fi(1); % Inicia como primera componente
    vx=1;
    while col<=n
        num=fi(2:(n-col+2),col-1)-fi(1:(n-col+1),col-1);
        den=(xi(col:n)-xi(1:(n-col+1)));
        fi(1:n-col+1,col)=num./den;
        vx=vx*(x-xi(col-1));
        p=p+fi(1,col)*vx;
        col=col+1;
    end
    b=fi(1,:);
end