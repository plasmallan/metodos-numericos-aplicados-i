fun=@f1;
x0=1;
tol=1e-100;
maxiter=10;
% [sol,iter,ACOC,incre1,incre2]=Newton(fun,x0,tol,maxiter);
% t=cputime;
% [sol,iter,ACOC,incre1,incre2]=NewtonNewton(fun,x0,tol,maxiter);
% t=cputime-t;
xm1=0.5;
t=cputime;
[sol,iter,ACOC,incre1,incre2]=Secante(fun,x0,xm1,tol,maxiter);
t=cputime-t;
function [f,df,ddf]=f1(x0)
    syms x
    f=sin(x)^2-x^2+1;
    df=diff(f,x,1);
    ddf=diff(f,x,2);
    f=vpa(subs(f,x,x0),5);
    df=vpa(subs(df,x,x0),5);
    ddf=vpa(subs(ddf,x,x0),5);
end