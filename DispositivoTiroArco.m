x=0:0.05:0.5;
F=[0 37 71 104 134 161 185 207 225 239 250];
m=0.075;
% Integrales
trapInt=IntegralTrapecio(x, F);
simpInt=Simpson(x, F);

% Cálculo de velocidades
vTrap=sqrt(2*trapInt/m);
vSimp=sqrt(2*simpInt/m);
[vS,vT]=Velocidad(x,F,m);

% Mostrar resultados
disp(['Integral por regla del trapecio (J): ', num2str(trapInt)]);
disp(['Integral Simpson 1/3 (J): ', num2str(simpInt)]);
disp(['Diferencia entre resultados (J): ', num2str(abs(trapInt-simpInt))]);
disp(['Velocidad a x=0.5 m con regla Trapecio (m/s): ', num2str(vTrap)]);
disp(['Velocidad a x=0.5 m con regla Simpson 1/3 (m/s): ', num2str(vSimp)]);
disp('Velocidades para cada x con regla Trapecio (m/s): ');
disp(vT)
disp('Velocidades para cada x con regla Simpson 1/3 (m/s): ');
disp(vS)
disp('Diferencias entre velocidades de ambos métodos (m/s): ');
disp(abs(vT-vS))


function [vS,vT] = Velocidad(x,y,m)
    % x: Vector de puntos en el eje x
    % y: Vector de valores de la función en los puntos y
    vS=zeros(1,length(x));
    vT=zeros(1,length(x));
    vS(1)=0;
    vT(1)=0;
    for i=2:length(x)
        x_sub=x(1:i);
        y_sub=y(1:i);
        wS(i)=Simpson(x_sub,y_sub);
        wT(i)=IntegralTrapecio(x_sub, y_sub);
        vS(i)=sqrt(2*wS(i)/m);
        vT(i)=sqrt(2*wT(i)/m);
    end
end
function integral = IntegralTrapecio(x, y)
    % x: Vector de puntos en el eje x
    % y: Vector de valores de la función en los puntos y
    
    if length(x) ~= length(y)
        error('Los vectores x e y deben tener la misma longitud.');
    end

    h = x(2) - x(1);
    
    integral = (h/2) * (2*sum(y) - y(1) - y(end));
end
function integral = Simpson(x, y)
    % x: Vector de puntos en el eje x
    % y: Vector de valores de la función en los puntos y
    
    n = length(x)-1;
    if length(x) ~= length(y)
        error('Los vectores x e y deben tener la misma longitud.');
    end
    if n < 4
        % Si n<4, no se puede usar Simpson, por lo que se usa Trapecio
        integral = IntegralTrapecio(x, y);
    elseif mod(n, 2) == 0
        h = x(2) - x(1);
        integral = (h/3) * (y(1) + 4*sum(y(2:2:n)) + 2*sum(y(3:2:n-1)) + y(n));
    else
        % Aplica Simpson 1/3 a primeros n-3 intervalos
        h = x(2) - x(1);
        k=n-3;
        integral = (h/3) * (y(1) + 4*sum(y(2:2:k)) + 2*sum(y(3:2:k-1)) + y(k));
        % Aplica Simpson 3/8 a últimos 3 intervalos
        integral = integral+(3*h/8) * (y(k) + 3*y(k+1) + 3*y(k+2) + y(n));
    end
end