% PVI ecuaciones diferenciales de primer orden
% Utilizando ODE23
h=0.1;

% Se define la variable independiente
a=0;b=3;N=8;
% Condiciones iniciales
y0=1;
[t,y]=Heun(@PVI,a,b,N,y0);
figure;
plot(t,y,'o-')

% Se define el sistema
function dy=PVI(t,y)
    dy=(1-2*t).*y;
end